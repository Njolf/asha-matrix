const { MatrixAuth, MatrixClient, SimpleFsStorageProvider, AutojoinRoomsMixin, RustSdkCryptoStorageProvider, EncryptionAlgorithm } = require("matrix-bot-sdk");
const fs = require("fs");
const showdown = require("showdown");
const striptags = require("striptags");
let path = require("path");
const TurndownService = require("turndown");

let parent = {};
parent.striptags = striptags;
parent.path = path;

const turndownServce = new TurndownService();

parent.turndownService = turndownServce;

const converter = new showdown.Converter();

parent.converter = converter;

const configFile = fs.readFileSync("bot-data/config.json");
let config = JSON.parse(configFile);

parent.config = config;

const requestedCommands = config.commands.split(',');
let existingCommands = new Map([]);

parent.commands = requestedCommands;

let client;

let dm = false;

const storage = new SimpleFsStorageProvider("bot-data/bot.json");
const crypto = new RustSdkCryptoStorageProvider("bot-data/crypto_thingies");

const homeserverUrl = config.homeserver;
const username = config.username;
const pass = config.pass;

parent.username = username;
parent.pass = pass;

const auth = new MatrixAuth(homeserverUrl);

let consoleLog = function (textToLog) {
    let logs = fs.readFileSync("logs/logs.txt");
    logs = logs + "\n (" + new Date().toISOString() + "): " + textToLog;
    console.log(textToLog);
    fs.writeFileSync("logs/final-log.txt", " (" + new Date().toISOString() + "): " + textToLog.toString());
    fs.writeFileSync("logs/logs.txt", logs);
}

parent.consoleLog = consoleLog;

let openPrivateChannel = async function (event, client, separateUser) {
    let users = fs.readFileSync("bot-data/users.json");
    let sender = separateUser || event.sender;
    users = JSON.parse(users);
    users[sender] = new Object();
    consoleLog("pravim sobu sa: ");
    consoleLog(sender);
    let newRoomId = await client.createRoom({
        invite: [sender],
        is_direct: true,
        visibility: "private",
        preset: "trusted_private_chat",
        initial_state: [
            { type: "m.room.encryption", state_key: "", content: { algorithm: EncryptionAlgorithm.MegolmV1AesSha2 } },
            { type: "m.room.guest_access", state_key: "", content: { guest_access: "can_join" } },
        ],
    });
    consoleLog("gotovo.");
    users[sender].chat = newRoomId;
    users[sender].chatMadeByUser = false;
    consoleLog("zapisujem izmene.");
    fs.writeFileSync("bot-data/users.json", JSON.stringify(users));
    return newRoomId;
}

parent.openPrivateChannel = openPrivateChannel;

let sendOnboardingMessage = function (roomToMessage, client) {
    let formatted_body = fs.readFileSync("modules/onboard/onboarding.md").toString();
    formatted_body = converter.makeHtml(formatted_body);
    let returnText = striptags(formatted_body);
    client.sendMessage(roomToMessage, {
        "msgtype": "m.text",
        "body": returnText,
        "format": "org.matrix.custom.html",
        "formatted_body": formatted_body,
    });
}

parent.sendOnboardingMessage = sendOnboardingMessage;

let leaveRoomIfEmpty = async function (room, client) {
    if (await (await client.getJoinedRoomMembers(room)).length < 2) {
        let users = fs.readFileSync("bot-data/users.json");
        users = JSON.parse(users);
        for (let user in users) {
            if (users[user].chat === room) {
                delete users[user];
                consoleLog(`${user} je imao DM, soba ${room}`);
                fs.writeFileSync("bot-data/users.json", JSON.stringify(users));
            }
        }
        await client.leaveRoom(room);
        consoleLog(`napustio ${room}; razlog: prazna`);
    }
}

parent.leaveRoomIfEmpty = leaveRoomIfEmpty;

let loadCommands = function () {
    let commandFiles = fs.readdirSync("modules");
    for (let commandFile of commandFiles) {
        if (requestedCommands.includes(commandFile)) {
            let command = require(`./modules/${commandFile}/${commandFile}.js`);
            existingCommands.set(commandFile, command);
        }
        else {
            consoleLog(`ignorisao "${commandFile}" direktoriju; nije zahtevano`);
        }
    }
    if (requestedCommands.length > existingCommands.size) {
        throw new Error("određeni zahtevani moduli se ne nalaze u /modules!");
    }
    consoleLog("učitao komande!");
}

let fixReplyFormat = function (textArr) {
    textArr = textArr[textArr.length - 1].split('\n');
    consoleLog(textArr);
    return textArr[textArr.length - 1];
}

if (!config.token) {
    auth.passwordLogin(username, pass).then(function (userId) {
        config.token = userId.accessToken;
        consoleLog("dobio token!");
        fs.writeFileSync("bot-data/config.json", JSON.stringify(config));
        consoleLog("zapisao token!");
    });
}

else {
    client = new MatrixClient(homeserverUrl, config.token, storage, crypto);
    AutojoinRoomsMixin.setupOnClient(client);
    client.start().then(async function () {
        consoleLog("bot uključen!");
        consoleLog("učitavam komande...");
        loadCommands();
        consoleLog("komande učitane!");
        consoleLog("napuštam prazne sobe...");
        for (let room of await client.getJoinedRooms()) {
            leaveRoomIfEmpty(room, client);
        }
        parent.client = client;
        consoleLog("gotovo!");
    });

    client.on("room.failed_decryption", function (roomId, event) {
        console.log("brišem crypto folder");
        fs.rmSync("bot-data/crypto_thingies", { recursive: true });
        process.exit(1);
    });

    client.on("room.event", async function (roomId, event) {
        if (!event.content || event.sender === `@${username}:matrix.org`) {
            return;
        }
        if (event.content.membership === "leave") {
            leaveRoomIfEmpty(roomId, client);
        }

        let users = fs.readFileSync("bot-data/users.json");
        users = JSON.parse(users);

        if (event.content.creator && event.type === "m.room.create") {
            if (users[event.sender] && users[event.sender].chat) {
                consoleLog(`${event.sender} je hteo da napravi prepisku. već postoji ${users[event.sender].chat}`);
                client.sendMessage(roomId, {
                    "msgtype": "m.text",
                    "body": `Već imate prepisku samnom, ID: ${users[event.sender].chat}`,
                });
                await client.leaveRoom(roomId);
                consoleLog(`napustio prepisku ${roomId}, razlog: Prepiska već postoji.`);
            }
            else {
                consoleLog(`${event.sender} je napravio prepisku samnom, ${roomId}`);
                users[event.sender] = new Object();
                users[event.sender].chat = roomId;
                users[event.sender].chatMadeByUser = true;
                let usersJson = JSON.stringify(users);
                fs.writeFileSync("bot-data/users.json", usersJson);
            }

        }

        if (event.content.membership === "join") {
            for (let user in users) {
                if (roomId === users[user].chat && await (await client.getJoinedRoomMembers(roomId)).length === 2 && !users[user].chatMadeByUser) {
                    sendOnboardingMessage(roomId, client);
                }
            }
            if (await (await client.getJoinedRoomMembers(roomId)).length === 3) {
                consoleLog(`${event.sender} je ušao u prepisku ${roomId}. više nije privatna.`);
                client.sendMessage(roomId, {
                    "msgtype": "m.text",
                    "body": "Ova prepiska više nije privatna.",
                });
                for (let user in users) {
                    if (users[user].chat === roomId) {
                        delete users[user];
                    }
                }
                let usersJson = JSON.stringify(users);
                fs.writeFileSync("bot-data/users.json", usersJson);
            }
            let onboardRooms = fs.readFileSync("modules/onboard/onboard-rooms.txt").toString();
            if (onboardRooms.includes(roomId)) {
                if (!users[event.sender]) {
                    let newRoomId = openPrivateChannel(event, client);
                    sendOnboardingMessage(newRoomId, client);
                }
            }
        }
    });

    client.on("room.message", async function (roomId, event) {
        if (!event.content || event.sender === `@${username}:matrix.org`) {
            return;
        }

        if (await (await client.getJoinedRoomMembers(roomId)).length > 2) {
            dm = false;
            parent.dm = dm;
        }

        else {
            dm = true;
            parent.dm = dm;
        }

        const sender = event.sender;
        parent.sender = sender;

        const body = event.content.body;
        parent.body = body;

        const recFormattedBody = event.content.formatted_body;
        parent.rec_formatted_body = recFormattedBody;

        parent.roomId = roomId;
        parent.event = event;
        parent.fs = fs;

        if (recFormattedBody && recFormattedBody.includes(`matrix.to/#/@${username}:matrix.org`)) {

            let text = body.replace(`${username}: `, '').trim();
            parent.text = text;

            let textAsArr = text.split(' ');
            parent.textAsArr = textAsArr;

            let attemptedCommand = textAsArr[0];

            if (recFormattedBody.includes("<mx-reply>")) {
                attemptedCommand = fixReplyFormat(textAsArr);
            }

            parent.attemptedCommand = attemptedCommand;

            if (requestedCommands.includes(attemptedCommand)) {
                existingCommands.get(attemptedCommand).execute(parent);
            }
            else {
                let tempCommString = requestedCommands[0];
                for (let i = 1; i < requestedCommands.length; i++) {
                    tempCommString = tempCommString + ", " + requestedCommands[i];
                }
                client.sendMessage(roomId, {
                    "msgtype": "m.text",
                    "body": `imate ${tempCommString}`,
                });
            }
        }
    });
}