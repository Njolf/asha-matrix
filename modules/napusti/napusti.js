module.exports = {
    execute: async function (parent) {
        let onboardRooms = parent.fs.readFileSync("modules/onboard/onboard-rooms.txt").toString();
        let users = parent.fs.readFileSync("bot-data/users.json");
        users = JSON.parse(users);
        if(onboardRooms.includes(parent.roomId)){
            onboardRooms = onboardRooms.replace(parent.roomId, '');
            parent.consoleLog(`${parent.roomId} je bila za onboard.`);
            parent.fs.writeFileSync("modules/onboard/onboard-rooms.txt", onboardRooms);
        }
        for(let user in users){
            if(users[user].chat === parent.roomId){
                delete users[user];
                parent.consoleLog(`${user} je imao prepisku ${parent.roomId}`);
                break;
            }
        }
        users = JSON.stringify(users);
        parent.fs.writeFileSync("bot-data/users.json", users);
        parent.consoleLog(`napuštam ${parent.roomId}, razlog: izbačen.`);
        parent.client.sendMessage(parent.roomId, {
            "msgtype": "m.text",
            "body": "Napuštam.",
        });
        parent.client.leaveRoom(parent.roomId);
        parent.consoleLog("gotovo.");
    }
}