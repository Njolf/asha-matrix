# `napusti` komanda:

Pri pozivu ove komande, bot napušta sobu. Ukoliko se soba nalazila među sobama za onboarding ili privatnim prepiskama sa osobama, briše se odatle.

# Primer: 

> `@bongobbot:matrix.org napusti`

> `Napuštam.`