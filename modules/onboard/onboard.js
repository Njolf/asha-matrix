module.exports = {
    execute: async function (parent) {
        let determineChannel = async function (users, parent, separateUser) {
            let sender = separateUser || parent.sender;
            if (users[sender] && users[sender].chat && !separateUser) {
                let roomId = users[sender].chat;
                parent.sendOnboardingMessage(roomId, parent.client);
            }
            else {
                parent.openPrivateChannel(parent.event, parent.client, sender);
            }
        }
        let users = parent.fs.readFileSync(parent.path.join(__dirname, "..", "..", "bot-data/users.json"));
        users = JSON.parse(users);
        if (!parent.textAsArr[1]) {
            determineChannel(users, parent);
        }
        else {
            let onboardRooms = parent.fs.readFileSync("modules/onboard/onboard-rooms.txt").toString();
            switch (parent.textAsArr[1]) {
                case "postavi":
                    if (onboardRooms.includes(parent.roomId)) {
                        parent.client.sendMessage(parent.roomId, {
                            "msgtype": "m.text",
                            "body": "Soba se već nalazi u sobama za onboarding.",
                        });
                    }
                    else {
                        onboardRooms = onboardRooms + parent.roomId;
                        parent.fs.writeFileSync("modules/onboard/onboard-rooms.txt", onboardRooms);
                        parent.consoleLog(`${parent.roomId} dodata u onboard sobe.`);
                        parent.client.sendMessage(parent.roomId, {
                            "msgtype": "m.text",
                            "body": "Soba uspešno dodata u sobe za onboarding.",
                        });
                    }
                    break;

                case "ukloni":
                    // let onboardRooms = parent.fs.readFileSync("onboard-rooms.txt").toString();
                    if (onboardRooms.includes(parent.roomId)) {
                        onboardRooms = onboardRooms.replace(parent.roomId, '');
                        parent.fs.writeFileSync("modules/onboard/onboard-rooms.txt", onboardRooms);
                        parent.consoleLog(`${parent.roomId} uklonjena iz onboard soba.`);
                        parent.client.sendMessage(parent.roomId, {
                            "msgtype": "m.text",
                            "body": "Soba uspešno uklonjena iz soba za onboarding.",
                        });
                    }
                    else {
                        parent.client.sendMessage(parent.roomId, {
                            "msgtype": "m.text",
                            "body": "Soba se trenutno ne nalazi u sobama za onboarding.",
                        });
                    }
                    break;

                case "ovde":
                    parent.sendOnboardingMessage(parent.roomId, parent.client);
                    break;

                default:
                    let roomMembers = await parent.client.getJoinedRoomMembers(parent.roomId);
                    delete roomMembers[roomMembers.indexOf(`@${parent.username}:matrix.org`)];
                    delete roomMembers[roomMembers.indexOf(parent.sender)];
                    for (let member of roomMembers) {
                        if (parent.rec_formatted_body.includes(member)) {
                            determineChannel(users, parent, member);
                        }
                    }
                    break;
            }
        }
    }
}