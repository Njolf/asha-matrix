# `onboard` komanda:

Osobi koja je pozvala ovu komandu pošalje podatke za onboarding u privatnu prepisku. Ukoliko ne postoji privatna prepiska sa tom osobom, otvori je. Ukoliko se prosledi član (ili niz članova) sobe posle `onboard` reči, podaci/privatna prepiska će biti usmereni ka toj osobi. Ukoliko se prosledi `postavi` posle `onboard`, soba će se smatrati onboarding sobom i bot će otvarati prepisku sa novim članovima. Ukoliko se prosledi `ukloni` posle `onboard`, soba se više neće smatrati sobom za onboarding. Ukoliko se prosledi `ovde`, samo će poslati tekst za onboarding u sobi gde se poruka nalazi, bez dodatnog pamćenja sobe ili korisnika.

# Primer:

> `@bongobbot:matrix.org onboard`

- **Zatim, u privatnoj prepisci:**

> `Onboarding text`
