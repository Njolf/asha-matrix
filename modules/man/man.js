module.exports = {
    execute: async function (parent) {
        let returnText;
        let formatted_body;
        let format;
        if (parent.textAsArr[1] && parent.commands.includes(parent.textAsArr[1])) {
            formatted_body = parent.fs.readFileSync(parent.path.join(__dirname, '..', '..', `modules/${parent.textAsArr[1]}/${parent.textAsArr[1]}.md`)).toString();
            formatted_body = parent.converter.makeHtml(formatted_body);
            returnText = parent.striptags(formatted_body);
            format = "org.matrix.custom.html";
        }
        else {
            let tempCommString = parent.commands[0];
            for (let i = 1; i < parent.commands.length; i++) {
                tempCommString = tempCommString + ", " + parent.commands[i];
            }
            parent.consoleLog(parent.commands);
            parent.consoleLog(tempCommString);
            returnText = `imate ${tempCommString}`;
        }
        parent.client.sendMessage(parent.roomId, {
            "msgtype": "m.text",
            "body": returnText,
            "format": format,
            "formatted_body": formatted_body,
        });
    }
}