# `man` komanda:

Ukoliko se pozove sa imenom postojeće komande posle `man`, vrati podatke o pomenutoj komandi. Ukoliko se pozove samostalno, ili uz pogrešno ime komande, ispiše dostupne komande.

# Primer:

>`@bongobbot:matrix.org man echo `

># `echo` komanda:
>
>vrati tekst koji se poslali u poruci, sa istim formatiranjem.
>
># Primer:
>
>`@bongobbot:matrix.org echo moj tekst`

>`moj tekst`
