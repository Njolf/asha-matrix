# `bilten` komanda:

Ispisuje trenutno sačuvan bilten. Ukoliko se prosledi parametar `dodaj`, prepisuje trenutno sačuvan bilten sadržajem poruke nakon parametra.

# Primer:

> `@bongobbot:matrix.org bilten dodaj pozdrav, ovo je bilten`

> `sadržaj poruke sačuvan kao bilten`
