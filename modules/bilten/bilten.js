module.exports = {
    execute: async function (parent) {
        let returnText;
        let formatted_body;
        let format;
        if (parent.textAsArr[1] && parent.textAsArr[1].includes("dodaj")) {
            parent.consoleLog("dodajem bilten");
            format = parent.event.content.format;
            parent.fs.writeFileSync("modules/bilten/bilten-format.txt", format);
            formatted_body = parent.rec_formatted_body.replace(`<a href="https://matrix.to/#/@${parent.username}:matrix.org">${parent.username}</a>:`, '');
            formatted_body = formatted_body.replace('bilten dodaj', '').trim();
            if (formatted_body.startsWith("<p> </p>")) {
                formatted_body = formatted_body.replace("<p> </p>", '');
            }
            parent.fs.writeFileSync("modules/bilten/tekst-biltena.html", formatted_body);
            parent.consoleLog(formatted_body);
            parent.fs.writeFileSync("modules/bilten/tekst-biltena.md", parent.turndownService.turndown(formatted_body));
            returnText = "sadržaj poruke sačuvan kao bilten";
            formatted_body = "";
        }
        else {
            parent.consoleLog("vraćam bilten");
            parent.consoleLog("komanda je bila:");
            parent.consoleLog(parent.textAsArr);
            let bilten = parent.fs.readFileSync("modules/bilten/tekst-biltena.html");
            formatted_body = bilten.toString().trim();
            returnText = parent.striptags(bilten.toString());
            format = parent.fs.readFileSync("modules/bilten/bilten-format.txt").toString();
        }
        parent.client.sendMessage(parent.roomId, {
            "msgtype": "m.text",
            "body": returnText,
            "format": format,
            "formatted_body": formatted_body,
        });
    }
}