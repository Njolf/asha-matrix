# `link` komanda:  

Ukoliko je poruka koja poziva ovu komandu odgovor na sliku, biće vraćen mxc link spomenute slike. Poruka mora da sadrži isključivo `link` komandu.

# Primer: 

> `@bongobbot:matrix.org link`

> `mxc://matrix.org/HyeWUUleChErqQTAPDfSkFoo`