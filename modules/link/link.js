module.exports = {
    execute: async function (parent) {
        let returnText;
        let formatted_body;
        let format;
        if (parent.event.content['m.relates_to']) {
            try {
                let eventId = parent.event.content['m.relates_to']['m.in_reply_to']['event_id'];
                let reffedEvent = await parent.client.getEvent(parent.roomId, eventId);
                let url;
                if (reffedEvent.content.file) {
                    url = reffedEvent.content.file.url;
                }
                else {
                    url = reffedEvent.content.url;
                }
                if (url && url.startsWith("mxc://")) {
                    formatted_body = `<a href=${url}>${url}</a>`;
                    format = "org.matrix.custom.html";
                    returnText = url;
                }
                else {
                    formatted_body = "<p>Došlo je do greške. Proverite poruku na koju ste odgovorili.</p>";
                    returnText = "Došlo je do greške. Proverite poruku na koju ste odgovorili.";
                }
            }
            catch (err) {
                parent.consoleLog(err);
            }
        }
        else {
            returnText = "odgovorite na neku sliku!";
        }
        parent.client.sendMessage(parent.roomId, {
            "msgtype": "m.text",
            "body": returnText,
            "format": format,
            "formatted_body": formatted_body,
        });
    }
}