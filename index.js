const forever = require("forever-monitor");

const child = new (forever.Monitor)('bot.js', {
    // silent: true,
    args: []
});

child.on('exit', function () {
    console.log("ponovo pokrećem bota");
    child.start(true);
    console.log("bot ponovo pokrenut");
});

console.log("pokrećem bota:");

child.start();

console.log("bot pokrenut");